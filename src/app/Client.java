package app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Scanner;
import java.util.StringTokenizer;

import message.GetHTTP;

public class Client {

    private static final String HOST = "localhost";
    private static final int SERVER_PORT = 8080;

    private BufferedReader dis;
    private PrintStream dos;

    public Client() {
        try {
            Socket socketCliente = new Socket(HOST, SERVER_PORT);
            this.dis = new BufferedReader(new InputStreamReader(socketCliente.getInputStream()));
            this.dos = new PrintStream(socketCliente.getOutputStream());

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    // TODO Si quisiera hacerlo mas generico y no fuera un HTML, como debería leer
    // del dos.
    public void manejarPeticionCliente(String datosEnviar) {
        try {
            datosEnviar = "/" + datosEnviar;

            // Enviamos la peticion del cliente al servidor
            GetHTTP rH = new GetHTTP();
            String peticionEnviar = rH.crearMensajeGetHTTP(datosEnviar);
            this.dos.println(peticionEnviar);

            System.out.println("\nDATOS RECIBIDOS: ");
            System.out.println(this.dis.readLine());

            // Leemos las cabeceras
            String aux;
            while (!(aux = this.dis.readLine()).isEmpty()) {
                System.out.println(aux);
            }

            // Leemos los datos
            while (!(aux = this.dis.readLine()).isEmpty()) {
                System.out.println(aux);
            }
            
        } catch (Exception e) {

        }
    }

    private int getContentLenght(String linea) {
        StringTokenizer st = new StringTokenizer(linea);
        st.nextToken();
        return Integer.valueOf(st.nextToken());
    }

    public static void main(String[] args) {
        Client cliente;
        Scanner scanner = new Scanner(System.in);

        System.out.println("¿Qué deseas realizar? [GET/POST]");
        String opcion = scanner.nextLine().toLowerCase();

        switch (opcion) {
            case "get":
                System.out.println("PETICION A ENVIAR AL SERVIDOR");
                String datosEnviar = scanner.nextLine();
                cliente = new Client();
                cliente.manejarPeticionCliente(datosEnviar);
                break;
            case "post":
                System.out.println("No implementado");
                break;
            default:
                System.out.println("Opción Incorrecta");
                break;
        }

    }

}