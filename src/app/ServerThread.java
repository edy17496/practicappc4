package app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.util.StringTokenizer;

import message.ResponseHTTP;

public class ServerThread extends Thread {

    private static int numCookie;

    private Socket socketThread;
    private BufferedReader dis;
    private PrintStream dos;

    public ServerThread(Socket socket) {
        try {
            this.socketThread = socket;
            this.dis = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.dos = new PrintStream(socket.getOutputStream());
            this.numCookie++;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public void run() {
        manejarPeticionesServidor();
    }

    public void manejarPeticionesServidor() {
        try {

            String datos = this.dis.readLine(); // GET / VERSION
            String aux;
            System.out.println("\nDATOS RECIBIDOS");
            System.out.println(datos);
            while (!(aux = this.dis.readLine()).isEmpty()) {
                System.out.println(aux);
            }

            ResponseHTTP responseHTTP = new ResponseHTTP();
            String response = responseHTTP.crearMensajeResponseHTTP(parsearPeticion(datos), this.numCookie);
            System.out.println("\nDATOS ENVIADOS");
            System.out.println(response);
            dos.println(response);


            this.dis.close();
            this.dos.close();
            this.socketThread.close();

        } catch (IOException e) {

            e.printStackTrace();
        }
    }

    private String parsearPeticion(String datos) {
        StringTokenizer st = new StringTokenizer(datos);
        st.nextToken(); //GET
        return st.nextToken();  //Recurso
    }

}
