package app;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;


/*
 * Lanzo el servidor, este escucha las peticiones que le llegan
 * cuando le llega una peticion el servidor se encarga de procesarla 
 * y enviar al cliente lo que ha pedido
 */
public class Server {

	private static final int SERVER_PORT = 8080;
	private ServerSocket sSocket;
	private Socket cliente;

	public Server() {
		try {
			this.sSocket = new ServerSocket(SERVER_PORT);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void atenderPeticiones() {
		while (true) {
			try {
				cliente = sSocket.accept();
				new ServerThread(cliente).start();

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) {
		System.out.println("Servidor Iniciado");

		Server server = new Server();
		
		server.atenderPeticiones();

		System.out.println("Servidor Finalizado");
	}

}
